<?php

namespace KominfoGusit\Metronic;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use KominfoGusit\Metronic\Components\Buttons\Confirm;
use KominfoGusit\Metronic\Components\Buttons\ModalForm;
use KominfoGusit\Metronic\Components\Collection\Filtering;
use KominfoGusit\Metronic\Components\Collection\Info;
use KominfoGusit\Metronic\Components\Collection\Pagination;
use KominfoGusit\Metronic\Components\Collection\RowPerPage;
use KominfoGusit\Metronic\Components\Form\DatePicker;
use KominfoGusit\Metronic\Components\Form\File;
use KominfoGusit\Metronic\Components\Form\Input;
use KominfoGusit\Metronic\Components\Form\Recaptcha2;
use KominfoGusit\Metronic\Components\Form\Select;
use KominfoGusit\Metronic\Components\Form\TestSubmit;
use KominfoGusit\Metronic\Components\Form\Textarea;

class Provider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /* ---------------------------------- views --------------------------------- */

        $this->loadViewsFrom(__DIR__ . '/views', 'metronic');

        /* ------------------------------- components ------------------------------- */

        Blade::component('form.input', Input::class);
        Blade::component('form.recaptcha2', Recaptcha2::class);
        Blade::component('form.file', File::class);
        Blade::component('form.select', Select::class);
        Blade::component('form.textarea', Textarea::class);
        Blade::component('form.testsubmit', TestSubmit::class);
        Blade::component('form.datepicker', DatePicker::class);

        Blade::component('collection.filtering', Filtering::class);
        Blade::component('collection.info', Info::class);
        Blade::component('collection.rowperpage', RowPerPage::class);
        Blade::component('collection.pagination', Pagination::class);

        Blade::component('buttons.modalform', ModalForm::class);
        Blade::component('buttons.confirm', Confirm::class);

        /* -------------------------------- validator ------------------------------- */

        Validator::extend('date_format_id', function ($attribute, $value, $parameters, $validator) {
            if (count($parameters) < 1) throw new Exception("Validator date_format_id harus memiliki parameter: format.");
            $lastError = null;
            $result = null;
            try {
                $result = Carbon::createFromLocaleFormat(head($parameters), "id", $value);
                $lastError = Carbon::getLastErrors();
            } catch (\Throwable $th) {
            }
            if ($lastError && $lastError["warning_count"] == 0 && $lastError["error_count"] == 0) return true;
            return false;
        });

        /* --------------------------------- publish -------------------------------- */

        $this->publishes([
            __DIR__ . '/config/metronic.php' => config_path('metronic.php'),
            __DIR__ . '/public' => public_path(),
            __DIR__ . '/lang' => lang_path(),
        ], 'metronic-1-required');

        $this->publishes([
            __DIR__ . '/views' => resource_path('views/vendor/metronic'),
        ], 'metronic-2-views');

        $this->publishes([
            __DIR__ . '/Components' => base_path('KominfoGusit/Metronic'),
        ], 'metronic-3-components');

        $this->publishes([
            __DIR__ . '/AppHelper.php' => base_path('KominfoGusit/Metronic'),
        ], 'metronic-4-helper');
    }
}
