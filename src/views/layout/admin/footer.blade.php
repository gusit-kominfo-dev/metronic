<div class="app-footer mt-10" id="kt_app_footer">
    <div class="app-container container-fluid d-flex flex-column flex-md-row flex-center flex-md-stack py-3">
        <div class="order-md-1 order-2 text-gray-900">
            Aplikasi Kepemerintahan<br>
            ©2024 Pemko Gunungsitoli
        </div>
    </div>
</div>
