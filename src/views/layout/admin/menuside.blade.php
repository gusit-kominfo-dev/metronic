<div class="menu-item">
    <a class="menu-link" href='#'>
        <span class="menu-icon">
            <i class="ki-solid ki-home"></i>
        </span>
        <span class="menu-title">Dashboard</span>
    </a>
</div>

<div class="menu-item">
    <a class="menu-link" href='#'>
        <span class="menu-icon">
            <i class="ki-solid ki-bank"></i>
        </span>
        <span class="menu-title">Menu 1</span>
    </a>
</div>

<div class="menu-item">
    <a class="menu-link" href='#'>
        <span class="menu-icon">
            <i class="ki-solid ki-abstract-26"></i>
        </span>
        <span class="menu-title">Menu 2</span>
    </a>
</div>

<div class="menu-item">
    <a class="menu-link" href='#'>
        <span class="menu-icon">
            <i class="ki-solid ki-book-square"></i>
        </span>
        <span class="menu-title">Menu 3</span>
    </a>
</div>

<div class="menu-item">
    <a class="menu-link" href='#'>
        <span class="menu-icon">
            <i class="ki-solid ki-monitor-mobile"></i>
        </span>
        <span class="menu-title">Menu 4</span>
    </a>
</div>
