<link href="{{ asset("metronic/img/favicon.png") }}" rel="apple-touch-icon">
<link type="image/png" href="{{ asset("metronic/img/favicon.png") }}" rel="icon">

<meta name="title" content="TITLE">
<meta name="description" content="DESCRIPTION">
<meta name="keywords" content="KEYWORDS">
{{-- <meta name="robots" content="index, follow"> --}}

<meta property="og:type" content="TYPE">
<meta property="og:url" content="URL">
<meta property="og:title" content="TITLE">
<meta property="og:description" content="DESCRIPTION">
<meta property="og:image" content="{{ asset("metronic/img/og-img.png") }}">

<meta property="twitter:card" content="CARD">
<meta property="twitter:url" content="URL">
<meta property="twitter:title" content="TITLE">
<meta property="twitter:description" content="DESCRIPTION">
<meta property="twitter:image" content="{{ asset("metronic/img/og-img.png") }}">
