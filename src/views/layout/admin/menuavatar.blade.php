<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold fs-6 w-275px py-4" data-kt-menu="true" style="">

    <div class="menu-item px-3">
        <div class="menu-content d-flex align-items-center px-3">

            <div class="symbol symbol-50px me-5">
                <img src="{{ asset("metronic/img/avatar.png") }}" alt="Logo">
            </div>

            <div class="d-flex flex-column">
                <div class="fw-bold d-flex align-items-center fs-5">User 1
                    <span class="badge badge-light-success fw-bold fs-8 ms-2 px-2 py-1">Pro</span>
                </div>
                <a class="fw-semibold text-muted text-hover-primary fs-7" href="javascript:void(0);">user@email.com</a>
            </div>

        </div>
    </div>

    <div class="separator my-2"></div>

    <div class="menu-item px-5">
        <a class="menu-link px-5" href="account/overview.html">My Profile</a>
    </div>

    <div class="menu-item px-5" data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="left-start" data-kt-menu-offset="-15px, 0">
        <a class="menu-link px-5" href="#">
            <span class="menu-title">My Subscription</span>
            <span class="menu-arrow"></span>
        </a>

        <div class="menu-sub menu-sub-dropdown w-175px py-4" style="">

            <div class="menu-item px-3">
                <a class="menu-link px-5" href="account/referrals.html">Referrals</a>
            </div>

            <div class="menu-item px-3">
                <a class="menu-link d-flex flex-stack px-5" href="account/statements.html">Statements
                    <span class="lh-0 ms-2" data-bs-toggle="tooltip" data-bs-original-title="View your statements" data-kt-initialized="1" aria-label="View your statements">
                        <i class="ki-duotone ki-information-5 fs-5">
                            <span class="path1"></span>
                            <span class="path2"></span>
                            <span class="path3"></span>
                        </i>
                    </span>
                </a>
            </div>

        </div>

    </div>

</div>
