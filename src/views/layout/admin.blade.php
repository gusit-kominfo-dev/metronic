<!DOCTYPE html>
<html lang="en">

@php
    $sidebarMinimized = false;
    if (isset($_COOKIE["sidebar_minimize_state"]) && $_COOKIE["sidebar_minimize_state"] === "on") {
        $sidebarMinimized = true;
    }
@endphp

{{-- Metronic 8.2.3 --}}

<head>
    @hasSection("page-title")
        <title>
            @yield("page-title") | {{ config("app.name") }}
        </title>
    @else
        <title>{{ config("app.name") }}</title>
    @endif
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    @include("metronic::layout.admin.htmlmeta")

    <link href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" rel="stylesheet" />

    <link type="text/css" href="{{ asset("assets/plugins/global/plugins.bundle.css") }}" rel="stylesheet" />
    <link type="text/css" href="{{ asset("assets/css/style.bundle.css") }}" rel="stylesheet" />
    <link type="text/css" href="{{ asset("metronic/css/custom.css") }}" rel="stylesheet" />

    <script>
        if (window.top != window.self) {
            window.top.location.replace(window.self.location.href);
        }
    </script>

    @stack("styles")
</head>

<body class="app-default" id="kt_app_body" data-kt-app-layout="dark-sidebar" data-kt-app-header-fixed="true" data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-hoverable="true" data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true" data-kt-app-sidebar-push-footer="true" data-kt-app-toolbar-enabled="true" @if ($sidebarMinimized) data-kt-app-sidebar-minimize="on" @endif>

    <script>
        var defaultThemeMode = "light";
        var themeMode;
        if (document.documentElement) {
            if (document.documentElement.hasAttribute("data-bs-theme-mode")) {
                themeMode = document.documentElement.getAttribute("data-bs-theme-mode");
            } else {
                if (localStorage.getItem("data-bs-theme") !== null) {
                    themeMode = localStorage.getItem("data-bs-theme");
                } else {
                    themeMode = defaultThemeMode;
                }
            }
            if (themeMode === "system") {
                themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
            }
            document.documentElement.setAttribute("data-bs-theme", themeMode);
        }
    </script>

    <div class="d-flex flex-column flex-root app-root" id="kt_app_root">

        <div class="app-page flex-column flex-column-fluid" id="kt_app_page">

            <div class="app-header" id="kt_app_header" data-kt-sticky="true" data-kt-sticky-activate="{default: true, lg: true}" data-kt-sticky-name="app-header-minimize" data-kt-sticky-offset="{default: '200px', lg: '0'}" data-kt-sticky-animation="false">

                <div class="app-container container-fluid d-flex align-items-stretch justify-content-between" id="kt_app_header_container">

                    <div class="d-flex align-items-center d-lg-none ms-n3 me-md-2 me-1" title="Show sidebar menu">
                        <div class="btn btn-icon btn-active-color-primary w-35px h-35px" id="kt_app_sidebar_mobile_toggle">
                            <i class="ki-duotone ki-abstract-14 fs-2 fs-md-1">
                                <span class="path1"></span>
                                <span class="path2"></span>
                            </i>
                        </div>
                    </div>

                    <div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
                        <a class="d-lg-none" href="{{ config("app.url") }}">
                            <img class="h-30px" src="{{ asset("metronic/img/app-logo-collapsed.png") }}" alt="Logo">
                        </a>
                    </div>

                    <div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1" id="kt_app_header_wrapper">

                        <div class="app-header-menu app-header-mobile-drawer align-items-stretch" data-kt-drawer="true" data-kt-drawer-name="app-header-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="250px" data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_app_header_menu_toggle" data-kt-swapper="true" data-kt-swapper-mode="{default: 'append', lg: 'prepend'}" data-kt-swapper-parent="{default: '#kt_app_body', lg: '#kt_app_header_wrapper'}" style=""></div>

                        <div class="app-navbar flex-shrink-0">

                            <div class="app-navbar-item ms-md-4 ms-1" id="kt_header_user_menu_toggle">

                                <div class="symbol symbol-35px cursor-pointer" data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                                    <img class="rounded-3" src="{{ asset("metronic/img/avatar.png") }}" alt="user">
                                </div>

                                @include("metronic::layout.admin.menuavatar")

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">

                <div class="app-sidebar flex-column" id="kt_app_sidebar" data-kt-drawer="true" data-kt-drawer-name="app-sidebar" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="225px" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_app_sidebar_mobile_toggle" style="">

                    <div class="app-sidebar-logo px-6" id="kt_app_sidebar_logo">

                        <a href="{{ config("app.url") }}">
                            <img class="h-25px app-sidebar-logo-default" src="{{ asset("metronic/img/app-logo.png") }}" alt="Logo">
                            <img class="h-20px app-sidebar-logo-minimize" src="{{ asset("metronic/img/app-logo-small.png") }}" alt="Logo">
                        </a>

                        <div class="app-sidebar-toggle btn btn-icon btn-shadow btn-sm btn-color-muted btn-active-color-primary h-30px w-30px position-absolute top-50 start-100 translate-middle rotate" id="kt_app_sidebar_toggle" data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body" data-kt-toggle-name="app-sidebar-minimize">
                            <i class="ki-duotone ki-black-left-line fs-3 rotate-180">
                                <span class="path1"></span>
                                <span class="path2"></span>
                            </i>
                        </div>

                    </div>

                    <div class="app-sidebar-menu flex-column-fluid overflow-hidden">

                        <div class="app-sidebar-wrapper" id="kt_app_sidebar_menu_wrapper">

                            <div class="scroll-y mx-3 my-5" id="kt_app_sidebar_menu_scroll" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_app_sidebar_logo, #kt_app_sidebar_footer" data-kt-scroll-wrappers="#kt_app_sidebar_menu" data-kt-scroll-offset="5px" data-kt-scroll-save-state="true" style="height: 630px;">

                                <div class="menu menu-column menu-rounded menu-sub-indention fw-semibold fs-6" id="#kt_app_sidebar_menu" data-kt-menu="true" data-kt-menu-expand="false">
                                    @include("metronic::layout.admin.menuside")
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="app-main flex-column flex-row-fluid" id="kt_app_main">

                    <div class="d-flex flex-column flex-column-fluid">

                        <div class="app-toolbar pb-lg-10 pt-lg-0 py-10" id="kt_app_toolbar">

                            <div class="app-container container-fluid d-flex flex-stack" id="kt_app_toolbar_container">

                                <div class="page-title d-flex flex-column justify-content-center me-3 flex-wrap">

                                    @hasSection("page-title")
                                        <h1 class="page-heading d-flex fw-bold fs-1 flex-column justify-content-center my-0 text-gray-900">
                                            @yield("page-title")
                                        </h1>
                                    @else
                                        <h1 class="page-heading d-flex fw-bold fs-1 flex-column justify-content-center my-0 text-gray-900">Metronic 8.2.3</h1>
                                    @endif

                                    @hasSection("breadcrumb")
                                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                                            @yield("breadcrumb")
                                        </ul>
                                    @endif

                                </div>

                            </div>

                        </div>

                        <div class="app-content flex-column-fluid" id="kt_app_content">

                            <div class="app-container container-fluid" id="kt_app_content_container">

                                @hasSection("content")
                                    @yield("content")
                                @endif

                            </div>

                        </div>

                    </div>

                    @include("metronic::layout.admin.footer")

                </div>

            </div>

        </div>

    </div>

    <div class="scrolltop" id="kt_scrolltop" data-kt-scrolltop="true">
        <i class="ki-duotone ki-arrow-up">
            <span class="path1"></span>
            <span class="path2"></span>
        </i>
    </div>

    @stack("modals")

    <script>
        var hostUrl = "assets/";
    </script>

    <script src="{{ asset("assets/plugins/global/plugins.bundle.js") }}"></script>
    <script src="{{ asset("assets/js/scripts.bundle.js") }}"></script>

    <script src="{{ asset("metronic/js/sweetalert.js") }}"></script>
    <script src="{{ asset("metronic/js/modal-form.js") }}"></script>

    @stack("scripts")

    @error("error_alert")
        <script>
            $(function() {
                Swal.fire('', '{{ $message }}', 'error');
            });
        </script>
    @enderror

</body>

</html>
