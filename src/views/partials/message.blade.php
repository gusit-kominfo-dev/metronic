<div class="alert bg-light-{{ $type }} border-{{ $type }} d-flex flex-column flex-sm-row border border-dashed">
    <i class="ki-duotone ki-search-list fs-2hx text-{{ $type }} mb-sm-0 mb-5 me-4"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>

    <div class="d-flex flex-column pe-sm-10 pe-0">
        <span>{{ $message }}</span>
    </div>
</div>

<div class="text-end">
    <a class="btn btn-sm btn-outline btn-outline-primary" id="closeButton" href="javascript:void(0);">Tutup</a>
</div>
