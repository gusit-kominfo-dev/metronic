<div class="component-container mb-4">
    <label class="form-label" for="{{ $name }}">
        {{ $displayLabel }}
        @if ($required)
            <span class="text-danger">*</span>
        @endif
    </label>

    <div class="input-group" id="{{ $name }}-input-group" data-td-target-input="nearest" data-td-target-toggle="nearest">
        <input class="form-control @if (!$disabled && !$readOnly) @error($name) is-invalid @else @if ($errors->any()) is-valid @endif @enderror @endif" id="{{ $name }}" name="{{ $name }}" data-td-target="#{{ $name }}-input-group" type="text" value="{{ old($name, $oldValue) }}" @if ($placeholder) placeholder="{{ $placeholder }}" @endif @if ($required) required @endif @if ($disabled) disabled @endif @if ($readOnly) readonly @endif {{ $attributes }} />
        <span class="input-group-text" data-td-target="#{{ $name }}-input-group" data-td-toggle="datetimepicker">
            <i class="ki-duotone ki-calendar fs-2"><span class="path1"></span><span class="path2"></span></i>
        </span>
    </div>

    <div class="validator-message-container">
        @if (!$disabled && !$readOnly)
            @error($name)
                <small class="validation-error d-block text-danger">{{ $message }}</small>
            @enderror
        @endif
    </div>

    @if ($displayHelpText)
        <small class='d-block text-muted' id='{{ $name . "." . "_help" }}'>{{ $displayHelpText }}</small>
    @endif

    @if ($modalForm)
        @php
            $maxDateRender = "";
            if ($maxDate == "-") {
                $maxDateRender = "";
            } else {
                $maxDateRender = "maxDate: new Date(" . ($maxDate == "" ? "" : '"' . $maxDate . '"') . "),";
            }
        @endphp
        <script>
            new tempusDominus.TempusDominus(document.getElementById("{{ $name }}"), {
                allowInputToggle: true,
                promptTimeOnDateChange: true,
                promptTimeOnDateChangeTransitionDelay: 500,
                restrictions: {
                    {{ $maxDateRender }}
                },
                localization: {
                    locale: 'id',
                    format: '{{ $format }}',
                },
                display: {
                    components: {
                        hours: {{ $enableHour ? "true" : "false" }},
                        minutes: {{ $enableMinute ? "true" : "false" }},
                        seconds: {{ $enableSecond ? "true" : "false" }},
                    },
                },
            });
        </script>
    @endif
</div>

@if (!$modalForm)
    @push("scripts")
        @php
            $maxDateRender = "";
            if ($maxDate == "-") {
                $maxDateRender = "";
            } else {
                $maxDateRender = "maxDate: new Date(" . ($maxDate == "" ? "" : '"' . $maxDate . '"') . "),";
            }
        @endphp
        <script>
            new tempusDominus.TempusDominus(document.getElementById("{{ $name }}"), {
                allowInputToggle: true,
                promptTimeOnDateChange: true,
                promptTimeOnDateChangeTransitionDelay: 500,
                restrictions: {
                    {{ $maxDateRender }}
                },
                localization: {
                    locale: 'id',
                    format: '{{ $format }}',
                },
                display: {
                    components: {
                        hours: {{ $enableHour ? "true" : "false" }},
                        minutes: {{ $enableMinute ? "true" : "false" }},
                        seconds: {{ $enableSecond ? "true" : "false" }},
                    },
                },
            });
        </script>
    @endpush
@endif
