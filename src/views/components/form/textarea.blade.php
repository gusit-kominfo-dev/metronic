<div class="component-container mb-4">
    <label class="form-label" for="{{ $name }}">
        {{ $displayLabel }}
        @if ($required)
            <span class="text-danger">*</span>
        @endif
    </label>

    <textarea class="form-control @if (!$disabled && !$readOnly) @error($name) is-invalid @else @if ($errors->any()) is-valid @endif @enderror @endif" id="{{ $name }}" name="{{ $name }}" @if ($placeholder) placeholder="{{ $placeholder }}" @endif @if ($rows) rows="{{ $rows }}" @endif @if ($cols) cols="{{ $cols }}" @endif @if ($required) required @endif @if ($disabled) disabled @endif @if ($readOnly) readonly @endif>{{ old($name, $oldValue) }}</textarea>

    <div class="validator-message-container">
        @if (!$disabled && !$readOnly)
            @error($name)
                <small class="validation-error d-block text-danger">{{ $message }}</small>
            @enderror
        @endif
    </div>

    @if ($displayHelpText)
        <small class='d-block text-muted' id='{{ $name . "." . "_help" }}'>{{ $displayHelpText }}</small>
    @endif
</div>
