<div class="component-container mb-4">
    <label class="form-label" for="{{ $name }}">
        {{ $displayLabel }}
        @if ($required)
            <span class="text-danger">*</span>
        @endif
    </label>

    <input class="form-control @if (!$disabled) @error($name) is-invalid @else @if ($errors->any()) is-valid @endif @enderror @endif" id="{{ $name }}" name="{{ $multiple ? $name . "[]" : $name }}" type="file" @if ($required) required @endif @if ($disabled) disabled @endif @if ($accept) accept="{{ $accept }}" @endif @if ($multiple) multiple @endif {{ $attributes }} />

    <div class="validator-message-container">
        @if (!$disabled)
            @error($name)
                <small class="validation-error d-block text-danger">{{ $message }}</small>
            @enderror
        @endif
    </div>

    @if ($displayHelpText)
        <small class='d-block text-muted' id='{{ $name . "." . "_help" }}'>{{ $displayHelpText }}</small>
    @endif
</div>
