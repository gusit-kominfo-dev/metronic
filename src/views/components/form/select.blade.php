<div class="component-container mb-4">
    <label class="form-label" for="{{ $name }}">
        {{ $displayLabel }}
        @if ($required)
            <span class="text-danger">*</span>
        @endif
    </label>

    <select class="form-control @if (!$disabled) @error($name) is-invalid @else @if ($errors->any()) is-valid @endif @enderror @endif form-select" id="{{ $name }}" name="{{ $name }}" @if ($size) size="{{ $size }}" @endif @if ($required) required @endif @if ($disabled) disabled @endif {{ $attributes }}>
        <option value="">{{ $placeholder }}</option>
        @if ($options)
            @if ($useGroup)
                @foreach ($options as $option)
                    <optgroup label="{{ $option["group"] }}">
                        @foreach ($option["options"] as $key => $value)
                            <option value="{{ $key }}" {{ old($name, $oldValue) == $key ? "selected" : "" }}>{{ $value }}</option>
                        @endforeach
                    </optgroup>
                @endforeach
            @else
                @foreach ($options as $key => $value)
                    <option value="{{ $key }}" {{ old($name, $oldValue) == $key ? "selected" : "" }}>{{ $value }}</option>
                @endforeach
            @endif
        @else
            {{ $slot }}
        @endif
    </select>

    <div class="validator-message-container">
        @if (!$disabled)
            @error($name)
                <small class="validation-error d-block text-danger">{{ $message }}</small>
            @enderror
        @endif
    </div>

    @if ($displayHelpText)
        <small class='d-block text-muted' id='{{ $name . "." . "_help" }}'>{{ $displayHelpText }}</small>
    @endif
</div>
