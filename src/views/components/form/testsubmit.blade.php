@if (app()->environment(["local"]))
    @php
        $attributes = $attributes->merge(["class" => "btn btn-danger"]);
    @endphp

    <button type="submit" {{ $attributes }}>Test</button>
@endif
