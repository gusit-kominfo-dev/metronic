@php
    if ($isTersaring) {
        $btnFilterAttr = $attributes->merge(["class" => "btn btn-filter btn-warning"]);
    } else {
        $btnFilterAttr = $attributes->merge(["class" => "btn btn-filter btn-secondary"]);
    }
    $modalBatalAttr = $attributes->merge(["class" => "btn btn-secondary"]);
    $modalResetAttr = $attributes->merge(["class" => "btn btn-secondary"]);
    $modalSaringAttr = $attributes->merge(["class" => "btn btn-primary"]);
@endphp

<a data-bs-toggle="modal" data-bs-target="#modalFilter" href="javascript:void(0);" title="{{ __("tooltip.btn_filter") }}" {{ $btnFilterAttr }}>
    <i class="fa-solid fa-filter p-0"></i>
</a>

<x-collection.info :$collection :$isTersaring :$postUrl></x-collection.info>

@pushOnce("modals")
    <div class="modal fade" id="modalFilter" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Saring/Cari Data</h5>
                    <button class="btn-close" data-bs-dismiss="modal" type="button" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="modalFilterForm" action="{{ $postUrl }}" method="GET">
                        {{ $slot }}
                        <div class="mt-5 pt-3 text-end">
                            <a data-bs-dismiss="modal" href="javascript:void(0);" {{ $modalBatalAttr }}>Batal</a>
                            <a href="{{ $postUrl }}" {{ $modalResetAttr }}>Reset</a>
                            <button type="submit" {{ $modalSaringAttr }}>Saring</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endPushOnce
