@php
    $attributes = $attributes->merge(["class" => "btn btn-secondary dropdown-toggle"]);
@endphp

<span class="dropdown">
    <a data-bs-toggle="dropdown" data-bs-popper-config='{"strategy":"fixed"}' href="javascript:void(0);" role="button" aria-expanded="false" {{ $attributes }}>
        {{ $selected }}
    </a>
    <ul class="dropdown-menu">
        @foreach ($options as $option)
            <li><a class="dropdown-item {{ $option == $selected ? "active" : "" }}" href="{{ request()->fullUrlWithQuery(["rowsperpage" => $option]) }}">{{ $option }} baris</a></li>
        @endforeach
    </ul>
</span>
