<a data-confirm-url='{{ $url }}' data-confirm-message="{{ $message }}" href="javascript:void(0);" onclick="requestConfirm(this)" {{ $attributes }}>
    {{ $slot }}
</a>

@pushOnce("scripts")
    @csrf
@endPushOnce
