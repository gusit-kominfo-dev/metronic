<a data-mf-id="modalForm" data-mf-url='{{ $url }}' data-mf-title="{{ $header }}" href="javascript:void(0);" onclick="modalFormShow(this)" {{ $attributes }}>
    {{ $slot }}
</a>

@pushOnce("modals")
    <div class="modal fade" id="modalForm" data-bs-backdrop="static" data-bs-keyboard="true" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title fs-5">Modal Form</h5>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
@endPushOnce
