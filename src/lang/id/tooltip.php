<?php

return [

    /* -------------------- tooltip standar, jangan di sentuh ------------------- */
    'btn_detail' => 'klik untuk melihat detail',
    'btn_create' => 'klik untuk menambah',
    'btn_edit' => 'klik untuk mengedit',
    'btn_save' => 'klik untuk menyimpan',
    'btn_delete' => 'klik untuk menghapus',
    'btn_cancel' => 'klik untuk membatalkan',
    'btn_back' => 'klik untuk kembali',
    'btn_filter' => 'klik untuk menyaring atau mencari data',

    /* ----------------- tooltip kustom, tambahkan di bawah ini ----------------- */

];
