<?php

namespace KominfoGusit\Metronic\Components\Form;

use Illuminate\View\Component;

class Textarea extends Component
{
    public string $name;
    public string $langContext;
    public string $placeholder;
    public string $helpText;
    public int $rows;
    public int $cols;

    public bool $required;
    public bool $disabled;
    public bool $readOnly;

    public mixed $oldValue;

    public string $displayLabel;
    public string $displayHelpText;

    public function __construct($name, $langContext, $placeholder = '', $helpText = '', $rows = 0, $cols = 0, $required = false, $disabled = false, $readOnly = false, $oldValue = null)
    {
        $this->name = $name;
        $this->langContext = $langContext;
        $this->placeholder = $placeholder;
        $this->helpText = $helpText;
        $this->rows = $rows;
        $this->cols = $cols;
        $this->required = $required;
        $this->disabled = $disabled;
        $this->readOnly = $readOnly;
        $this->oldValue = $oldValue;

        $this->displayLabel = __($langContext . "." . $name);
        if (!$helpText || $helpText == '') {
            $context = $langContext . "." . $name . "_help";
            $helpText = __($context);
            if ($helpText != $context) {
                $this->displayHelpText = __($langContext . "." . $name . "_help");
            } else {
                $this->displayHelpText = '';
            }
        } else {
            $this->displayHelpText = $helpText;
        }
    }

    public function render()
    {
        return view('metronic::components.form.textarea');
    }
}
