<?php

namespace KominfoGusit\Metronic\Components\Form;

use Illuminate\View\Component;

class DatePicker extends Component
{
    public string $name;
    public string $label;
    public string $langContext;
    public string $placeholder;
    public string $helpText;

    public bool $required;
    public bool $disabled;
    public bool $readOnly;

    public mixed $oldValue;

    public string $maxDate;
    public string $format;
    public bool $enableHour;
    public bool $enableMinute;
    public bool $enableSecond;

    public string $displayLabel;
    public string $displayHelpText;

    public bool $modalForm;

    public function __construct(string $name, string $langContext, string $label = '', string $placeholder = '', string $helpText = '', bool $required = false, bool $disabled = false, bool $readOnly = false, mixed $oldValue = null, string $maxDate = '', string $format = 'yyyy-MM-dd', bool $enableHour = false, bool $enableMinute = false, bool $enableSecond = false, bool $modalForm = true)
    {
        $this->name = $name;
        $this->label = $label;
        $this->langContext = $langContext;
        $this->placeholder = $placeholder;
        $this->helpText = $helpText;
        $this->required = $required;
        $this->disabled = $disabled;
        $this->readOnly = $readOnly;
        $this->oldValue = $oldValue;

        $this->maxDate = $maxDate;
        $this->format = $format;
        $this->enableHour = $enableHour;
        $this->enableMinute = $enableMinute;
        $this->enableSecond = $enableSecond;

        $this->modalForm = $modalForm;

        $this->displayLabel = $this->label == '' ? __($langContext . "." . $name) : $this->label;
        if (!$helpText || $helpText == '') {
            $context = $langContext . "." . $name . "_help";
            $helpText = __($context);
            if ($helpText != $context) {
                $this->displayHelpText = __($langContext . "." . $name . "_help");
            } else {
                $this->displayHelpText = '';
            }
        } else {
            $this->displayHelpText = $helpText;
        }
    }

    public function render()
    {
        return view('metronic::components.form.datepicker');
    }
}
