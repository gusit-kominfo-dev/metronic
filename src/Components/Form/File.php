<?php

namespace KominfoGusit\Metronic\Components\Form;

use Illuminate\View\Component;

class File extends Component
{
    public string $name;
    public string $langContext;
    public string $helpText;

    public bool $required;
    public bool $disabled;

    public string $accept;
    public bool $multiple;

    public string $displayLabel;
    public string $displayHelpText;

    public function __construct($name, $langContext, $helpText = '', $required = false, $disabled = false, $accept = '', $multiple = false)
    {
        $this->name = $name;
        $this->langContext = $langContext;
        $this->helpText = $helpText;
        $this->required = $required;
        $this->disabled = $disabled;
        $this->accept = $accept;
        $this->multiple = $multiple;

        $this->displayLabel = __($langContext . "." . $name);
        if (!$helpText || $helpText == '') {
            $context = $langContext . "." . $name . "_help";
            $helpText = __($context);
            if ($helpText != $context) {
                $this->displayHelpText = __($langContext . "." . $name . "_help");
            } else {
                $this->displayHelpText = '';
            }
        } else {
            $this->displayHelpText = $helpText;
        }
    }

    public function render()
    {
        return view('metronic::components.form.file');
    }
}
