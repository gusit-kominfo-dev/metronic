<?php

namespace KominfoGusit\Metronic\Components\Form;

use Illuminate\View\Component;

class TestSubmit extends Component
{
    public function __construct()
    {
        //
    }

    public function render()
    {
        return view("metronic::components.form.testsubmit");
    }
}
