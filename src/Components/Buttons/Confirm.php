<?php

namespace KominfoGusit\Metronic\Components\Buttons;

use Illuminate\View\Component;

class Confirm extends Component
{
    public string $url;
    public string $message;

    public function __construct(string $url, string $message)
    {
        $this->url = $url;
        $this->message = $message;
    }

    public function render()
    {
        return view('metronic::components.buttons.confirm');
    }
}
