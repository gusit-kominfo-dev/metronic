<?php

namespace KominfoGusit\Metronic\Components\Buttons;

use Illuminate\View\Component;

class ModalForm extends Component
{
    public string $url;
    public string $header;

    public function __construct(string $url, string $header)
    {
        $this->url = $url;
        $this->header = $header;
    }

    public function render()
    {
        return view('metronic::components.buttons.modalform');
    }
}
