<?php

namespace KominfoGusit\Metronic\Components\Collection;

use Illuminate\View\Component;

class RowPerPage extends Component
{
    public array $options;

    public int $selected;

    public function __construct()
    {
        $this->options = config('metronic.rows_per_page_options');
        $this->selected = intval(request()->query('rowsperpage', request()->cookie('rowsperpage', config('metronic.rows_per_page'))));
    }

    public function render()
    {
        return view('metronic::components.collection.rowperpage');
    }
}
