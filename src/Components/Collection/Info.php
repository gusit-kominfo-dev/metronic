<?php

namespace KominfoGusit\Metronic\Components\Collection;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\View\Component;

class Info extends Component
{
    public mixed $collection;
    public bool $isTersaring;
    public string $postUrl;

    public int $dataTotal;

    public function __construct($collection, $isTersaring = false, $postUrl = '')
    {
        $this->collection = $collection;
        $this->isTersaring = $isTersaring;
        $this->postUrl = $postUrl;

        $this->dataTotal = 0;
        if ($collection instanceof LengthAwarePaginator) {
            $this->dataTotal = $collection->total();
        } elseif ($collection instanceof Collection) {
            $this->dataTotal = $collection->count();
        } else {
            throw new Exception("Component Collection/Info tidak bisa memproses: " . gettype($collection));
        }
    }

    public function render()
    {
        return view('metronic::components.collection.info');
    }
}
