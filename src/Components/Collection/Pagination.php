<?php

namespace KominfoGusit\Metronic\Components\Collection;

use Illuminate\View\Component;

class Pagination extends Component
{
    public mixed $objectList;

    public function __construct(mixed $objectList)
    {
        $this->objectList = $objectList;
    }

    public function render()
    {
        return view('metronic::components.collection.pagination');
    }
}
